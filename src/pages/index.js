import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram, faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <Head>
        <title>Daniel Gillette</title>
        <meta name="description" content="Daniel Gillette's Personal Website" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.jpg" />
      </Head>
      <main className={styles.main}>
        <div className={styles.center}>
          <div>
            <Image
              src="/danielgillette-alianamt.jpg"
              alt="Daniel Gillette by aliana mt"
              width={1535/4}
              height={2279/4}
              priority
            />
          </div>
          <div className={styles.description}>
            <p>
              Hi.
            </p>
            <p>
              My name is Daniel and I’m a Software Developer. If that sounds about right, then you’re probably in the right place.
            </p>
            <p>
              I’m currently spending my free time volunteering with Citizen’s Climate Lobby and searching for project ideas centered around technology and tools to help in the fight against climate change.
            </p>
            <p>
              If you have any ideas or would simply like to collaborate, please don’t hesitate to reach out. In the meantime, you can always let your representatives know how important climate change is to you. CCL makes that fairly easy <a href="https://citizensclimatelobby.org/senate-write/">here</a>.
            </p>
            <p>
              Thanks for stopping by. Cheers.
            </p>
            {/* <FontAwesomeIcon icon={faGitlab} /> */}
            <p>
              <a href="http://instagram.com/dwgillette">
                <FontAwesomeIcon className={styles.icon} size="lg" icon={faInstagram} />
              </a>
              <a href="https://www.linkedin.com/in/dwgillette/">
                <FontAwesomeIcon className={styles.icon} size="lg" icon={faLinkedin} />
              </a>
              <a href="mailto:daniel.w.gillette@gmail.com">
                <FontAwesomeIcon className={styles.icon} size="lg" icon={faEnvelope} />
              </a>
            </p>
          </div>
        </div>
      </main>
    </>
  )
}
